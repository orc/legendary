module.exports = {
  plugins: [
    require("postcss-import")({
      plugins: [require("stylelint")()],
    }),
    require("tailwindcss"),
    require("autoprefixer"),
    require("cssnano")({
      preset: "default",
    }),
    require("postcss-color-function")(),
  ],
};
