defmodule AppWeb.LiveHelpers do
  @moduledoc """
  Commonly functions for LiveViews.
  """
  import Phoenix.LiveView
  import Phoenix.LiveView.Helpers

  def assign_defaults(socket, session) do
    assign_new(socket, :current_user, fn -> get_user(socket, session) end)
  end

  def require_auth(socket) do
    case socket.assigns do
      %{current_user: user} when not is_nil(user) ->
        socket
      _ ->
        redirect(socket, to: "/")
    end
  end

  defp get_user(socket, session, config \\ [otp_app: :core])

  defp get_user(socket, %{"core_auth" => signed_token}, config) do
    {otp_app, _config} = Keyword.pop(config, :otp_app, :core)
    {store, store_config} = Pow.Plug.Base.store(Application.get_env(otp_app, :pow))

    conn = struct!(Plug.Conn, secret_key_base: socket.endpoint.config(:secret_key_base))
    salt = Atom.to_string(Pow.Plug.Session)

    with {:ok, token} <- Pow.Plug.verify_token(conn, salt, signed_token, config),
        {user, _metadata} <- store.get(store_config, token) do
      user
    else
      _any -> nil
    end
  end

  defp get_user(_, _, _), do: nil

  @doc """
  Renders a component inside the `AppWeb.ModalComponent` component.

  The rendered modal receives a `:return_to` option to properly update
  the URL when the modal is closed.

  ## Examples

      <%= live_modal AppWeb.ItemLive.FormComponent,
        id: @item.id || :new,
        action: @live_action,
        item: @item,
        return_to: Routes.item_index_path(@socket, :index) %>
  """
  def live_modal(component, opts) do
    path = Keyword.fetch!(opts, :return_to)
    modal_opts = [id: :modal, return_to: path, component: component, opts: opts]
    live_component(AppWeb.ModalComponent, modal_opts)
  end
end
