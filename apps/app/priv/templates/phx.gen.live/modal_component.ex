defmodule <%= inspect context.web_module %>.ModalComponent do
  use <%= inspect context.web_module %>, :live_component

  @impl true
  def render(assigns) do
    assigns = Map.put(assigns, :title, Keyword.get(assigns.opts, :title))
    ~H"""
    <div
      id={@id}
      class="phx-modal absolute inset-0 flex items-center justify-center bg-gray-700 bg-opacity-50"
      phx-capture-click="close"
      phx-window-keydown="close"
      phx-key="escape"
      phx-target={@myself}
      phx-page-loading>

      <div class="phx-modal-content p-6 mx-auto max-w-2xl w-3/4 bg-gray-100 bg-opacity-100 shadow-lg rounded-lg">
        <div class="p-6 mx-auto max-w-2xl">
          <div class="flex pb-6">
            <div class="w-/12 flex-1 text-4xl">
              <h1><%%= @title %></h1>
            </div>
            <div class="w-/12 text-4xl text-right">
              <%%= styled_button_live_patch "Back", to: @return_to, class: "phx-modal-close"%>
            </div>
          </div>
          <%%= live_component @component, @opts %>
        </div>
      </div>
    </div>
    """
  end

  @impl true
  def handle_event("close", _, socket) do
    {:noreply, push_patch(socket, to: socket.assigns.return_to)}
  end
end
